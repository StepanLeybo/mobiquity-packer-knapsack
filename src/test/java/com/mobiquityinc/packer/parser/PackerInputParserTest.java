package com.mobiquityinc.packer.parser;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.PackTask;
import com.mobiquityinc.packer.PackThing;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PackerInputParserTest {
    private final PackerInputParser packerInputParser = new PackerInputParser();

    PackThing[] expectedPackThings = {
            new PackThing(6, new BigDecimal("46.34"), new BigDecimal("4")),
            new PackThing(1, new BigDecimal("53.38"), new BigDecimal("45")),
            new PackThing(2, new BigDecimal("88.62"), new BigDecimal("98"))
    };

    @Test
    public void parseTest() {
        List<PackTask> packTasks = packerInputParser.parse("src/test/resources/ValidInput.txt");

        assertEquals(2, packTasks.size());
        assertTrue(Arrays.equals(expectedPackThings, packTasks.get(0).getPackThings()));
        assertEquals(new BigDecimal("81"), packTasks.get(0).getPackSize());
        assertTrue(Arrays.equals(expectedPackThings, packTasks.get(1).getPackThings()));
        assertEquals(new BigDecimal("84"), packTasks.get(1).getPackSize());
    }

    @Test
    public void testParseLine() throws Exception {
        PackTask packTask = packerInputParser.parsePackTask("81 : (1,53.38,€45) (2,88.62,€98) (6,46.34,€4)");

        assertTrue(Arrays.equals(expectedPackThings, packTask.getPackThings()));
        assertEquals(new BigDecimal("81"), packTask.getPackSize());
    }

    @Test(expected = APIException.class)
    public void testFileNotExist() throws Exception {
        packerInputParser.parse("/file/not/exist");
    }

    @Test(expected = APIException.class)
    public void testInvalidLineInput1() throws Exception {
        packerInputParser.parsePackTask("81 : (1,53.38,€45) (2,88.62,€98) (6,46.34,€4");
    }

    @Test(expected = APIException.class)
    public void testInvalidLineInput2() throws Exception {
        packerInputParser.parsePackTask("9: (53.38,€45) (88.62,€98) (6,46.34,€4)");
    }

    @Test(expected = APIException.class)
    public void testInvalidLineInput3() {
        packerInputParser.parsePackTask("12: (1,53.38,) (2,88.62,€98) (6,46.34,€4)");
    }

    @Test(expected = APIException.class)
    public void testInvalidLineInput4() {
        packerInputParser.parsePackTask("3: (1,€45) (2,88.62,€98) (6,46.34,€4)");
    }

    @Test(expected = APIException.class)
    public void testInvalidPackWeight() {
        packerInputParser.parsePackTask("100.1: (1,53.38,€45) (2,88.62,€98) (6,46.34,€4)");
    }

    @Test(expected = APIException.class)
    public void testInvalidThingWeight() {
        packerInputParser.parsePackTask("10: (1,153.38,€45) (2,88.62,€98) (6,46.34,€4)");
    }

    @Test(expected = APIException.class)
    public void testInvalidThingPrice() {
        packerInputParser.parsePackTask("10: (1,53.38,€45) (2,88.62,€101) (6,46.34,€4)");
    }

    @Test(expected = APIException.class)
    public void testInvalidThingsSize() {
        packerInputParser.parsePackTask("10: (1,53.38,€45) (2,88.62,€10) (6,46.34,€4) (1,53.38,€45) (2,88.62,€10) (6,46.34,€4) (1,53.38,€45) (2,88.62,€10) (6,46.34,€4) (1,53.38,€45) (2,88.62,€10) (6,46.34,€4) (1,53.38,€45) (2,88.62,€10) (6,46.34,€4) (1,53.38,€45)");
    }
}
