package com.mobiquityinc.packer.selection.impl;


import com.mobiquityinc.packer.PackTask;
import com.mobiquityinc.packer.parser.PackerInputParser;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class KnapsackPackSelectorServiceTest {

    private final PackerInputParser packerInputParser = new PackerInputParser();
    private final KnapsackPackSelectorService packSelector = new KnapsackPackSelectorService();

    @Test
    public void testAlgorithm1() {
        PackTask task = packerInputParser.parsePackTask("81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)");
        int[] items = packSelector.selectPackThings(task);

        assertTrue(Arrays.equals(new int[]{4}, items));
    }

    @Test
    public void testAlgorithm2() {
        PackTask task = packerInputParser.parsePackTask("8 : (1,15.3,€34)");
        int[] items = packSelector.selectPackThings(task);

        assertTrue(Arrays.equals(new int[]{}, items));
    }

    @Test
    public void testAlgorithm3() {
        PackTask task = packerInputParser.parsePackTask("56 : (1,56,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) (5,46.81,€36) (6,48.77,€79) (7,81.80,€45) (8,19.36,€79) (9,6.76,€64)");
        int[] items = packSelector.selectPackThings(task);

        assertTrue(Arrays.equals(new int[]{8, 9}, items));
    }

    @Test
    public void testAlgorithm4() {
        PackTask task = packerInputParser.parsePackTask("75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) (6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78)");
        int[] items = packSelector.selectPackThings(task);

        assertTrue(Arrays.equals(new int[]{2, 7}, items));
    }

    @Test
    public void testAlgorithm5() {
        PackTask task = packerInputParser.parsePackTask("6 : (1,3,€3) (2,5,€5) (3,3,€3) (4,2,€2)");
        int[] items = packSelector.selectPackThings(task);

        assertTrue(Arrays.equals(new int[]{1, 3}, items));
    }
}