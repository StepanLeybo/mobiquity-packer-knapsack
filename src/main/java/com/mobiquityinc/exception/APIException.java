package com.mobiquityinc.exception;

/***
 * The exception is thrown on incorrect input
 */
public class APIException extends RuntimeException {
    public APIException(String message) {
        super(message);
    }
}
