package com.mobiquityinc.packer.selection;

import com.mobiquityinc.packer.PackTask;

/**
 * Select package items from the array of items
 * Interface is used as we are likely to have multiple implementations of pack items selection algorithm
 */
public interface PackSelectorService {
    /**
     *
     * @param items array of all available things
     * @return array of indexes of things selected for the pack
     */
    int[] selectPackThings(PackTask items);
}
