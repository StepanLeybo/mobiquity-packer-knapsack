package com.mobiquityinc.packer.selection.impl;

import com.mobiquityinc.packer.PackTask;
import com.mobiquityinc.packer.selection.PackSelectorService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Use knapsack algorithm to calculate best solution
 */
public class KnapsackPackSelectorService implements PackSelectorService {
    private static final BigDecimal HUNDRED = new BigDecimal(100);

    @Override
    public int[] selectPackThings(PackTask task) {
        // assume we have 2 digits after decimal point
        int[][] knapsackMatrix = getKnapsackMatrix(task);
        List<Integer> selectedIndexes = getSelectedIndexes(knapsackMatrix, task);

        // find things from matrix indexes
        return selectedIndexes.stream()
                .mapToInt(index -> task.getPackThings()[index-1].getIndex())
                .sorted()
                .toArray();
    }

    /**
     * generate knapsack matrix from task
     * @param task pack task
     * @return knapsack matrix
     */
    private int[][] getKnapsackMatrix(PackTask task) {
        // assume that all weights have 2 digits after decimal point
        int knapsackWeight = task.getPackSize().multiply(HUNDRED).intValue() + 1;
        int[][] knapsackMatrix = new int[task.getPackThings().length + 1][knapsackWeight ];
        for (int i = 0; i < knapsackWeight; i++) {
            knapsackMatrix[0][i] = 0;
        }

        for (int itemIndex = 1; itemIndex <= task.getPackThings().length; itemIndex++) {
            int thingWeight = task.getPackThings()[itemIndex - 1].getWeight().multiply(HUNDRED).intValue();
            int thingPrice = task.getPackThings()[itemIndex - 1].getPrice().multiply(HUNDRED).intValue();
            for (int weightIndex = 0; weightIndex < knapsackWeight; weightIndex++) {
                knapsackMatrix[itemIndex][weightIndex] = calculateKnapsackValue(
                        knapsackMatrix,
                        thingWeight,
                        thingPrice,
                        itemIndex,
                        weightIndex);
            }
        }

        return knapsackMatrix;
    }

    private int calculateKnapsackValue(int[][] knapsackMatrix, int thingWeight, int thingPrice, int itemIndex, int weightIndex) {
        int previousKnapsackValue = getKnapsackMatrixValue(knapsackMatrix, itemIndex - 1, weightIndex).orElse(0);
        Optional<Integer> possibleValueOpt = getKnapsackMatrixValue(knapsackMatrix, itemIndex - 1, weightIndex - thingWeight);
        int possibleValue = possibleValueOpt.map(integer -> integer + thingPrice).orElse(0);

        return Math.max(previousKnapsackValue, possibleValue);
    }

    // we can slightly improve performance by returning null instead of optional
    private Optional<Integer> getKnapsackMatrixValue(int[][] knapsackMatrix, int itemIndex, int weightIndex) {
        if (itemIndex < 0 || weightIndex < 0) {
            return Optional.empty();
        }

        return Optional.of(knapsackMatrix[itemIndex][weightIndex]);
    }

    /**
     * Get selected item indexes from the matrix
     * @param knapsackMatrix knapsack matrix
     * @return indexes of selected elements
     */
    private List<Integer> getSelectedIndexes(int[][] knapsackMatrix, PackTask task) {
        int itemIndex = knapsackMatrix.length - 1;
        int weightIndex = knapsackMatrix[0].length - 1;

        int knapsackWeight = knapsackMatrix[itemIndex][weightIndex];
        if (knapsackWeight == 0) {
            return Collections.emptyList();
        }

        List<Integer> itemsInKnapsack = new ArrayList<>();
        while (itemIndex > 0) {
            Optional<Integer> knapsackMatrixValue = getKnapsackMatrixValue(knapsackMatrix, itemIndex - 1, weightIndex);

            if (knapsackMatrixValue.isPresent()) {
                if (knapsackMatrixValue.get() != knapsackWeight) {
                    itemsInKnapsack.add(itemIndex);
                    weightIndex -= task.getPackThings()[itemIndex - 1].getWeight().multiply(HUNDRED).intValue();
                    knapsackWeight = getKnapsackMatrixValue(knapsackMatrix, itemIndex - 1, weightIndex).orElse(0);
                }
            }
            itemIndex--;
        }
        return itemsInKnapsack;
    }
}
