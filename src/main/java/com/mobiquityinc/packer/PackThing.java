package com.mobiquityinc.packer;

import com.google.common.base.Objects;

import java.math.BigDecimal;

public class PackThing {
    private final int index;
    private final BigDecimal weight;
    private final BigDecimal price;

    public PackThing(int index, BigDecimal weight, BigDecimal price) {
        this.index = index;
        this.weight = weight;
        this.price = price;
    }

    public int getIndex() {
        return index;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PackThing packThing = (PackThing) o;
        return index == packThing.index &&
                Objects.equal(weight, packThing.weight) &&
                Objects.equal(price, packThing.price);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(index, weight, price);
    }
}
