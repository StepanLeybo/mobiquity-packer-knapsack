package com.mobiquityinc.packer;

import com.google.common.base.Objects;

import java.math.BigDecimal;

/**
 * Represents one task to calculate
 */
public class PackTask {
    private final BigDecimal packSize;
    private final PackThing[] packThings;

    public PackTask(BigDecimal packSize, PackThing[] packThings) {
        this.packSize = packSize;
        this.packThings = packThings;
    }

    public BigDecimal getPackSize() {
        return packSize;
    }

    public PackThing[] getPackThings() {
        return packThings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PackTask packTask = (PackTask) o;
        return Objects.equal(packSize, packTask.packSize) &&
                Objects.equal(packThings, packTask.packThings);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(packSize, packThings);
    }
}
