package com.mobiquityinc.packer;

import com.mobiquityinc.packer.parser.PackerInputParser;
import com.mobiquityinc.packer.selection.PackSelectorService;
import com.mobiquityinc.packer.selection.impl.KnapsackPackSelectorService;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

public class Packer {
    // Can be replaced with IOC in non static class, no good reason to add IOC here
    private static final PackSelectorService packSelectorService = new KnapsackPackSelectorService();
    private static final PackerInputParser packerInputParser = new PackerInputParser();

    /**
     * Find optimal solution for things packing problem
     *
     * @param inputFilePath path to file with data
     * @return numbers of items to pack, one line per solution
     */
    public static String pack(String inputFilePath) {
        List<PackTask> tasks = packerInputParser.parse(inputFilePath);
        List<String> results = tasks.stream()
                .map(task -> {
                    int[] packThingIndexes = packSelectorService.selectPackThings(task);
                    return packThingIndexes.length != 0
                            ? StringUtils.join(packThingIndexes, ',')
                            : "-";
                })
                .collect(Collectors.toList());

        return StringUtils.join(results, '\n');
    }
}
