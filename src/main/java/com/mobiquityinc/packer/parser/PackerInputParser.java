package com.mobiquityinc.packer.parser;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.PackTask;
import com.mobiquityinc.packer.PackThing;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PackerInputParser {
    private final Pattern lineValidationRegex = Pattern.compile("[\\d.]+( ?):(( ?)\\(([\\d\\.]+,){2}([\\d\\.€]+){1}\\))+");
    private final Pattern thingRegex = Pattern.compile("([\\d\\.]+,){2}([\\d\\.€]+){1}");

    public List<PackTask> parse(String filePath) {
        if (!new File(filePath).isFile()) {
            throw new APIException(String.format("Can't find file with path: %s", filePath));
        }

        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            return stream.map(this::parsePackTask).collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public PackTask parsePackTask(String line) {
        // check that line has a valid structure
        Matcher validationMatcher = lineValidationRegex.matcher(line);
        if (!validationMatcher.find() || !validationMatcher.group().equals(line)) {
            throw new APIException(String.format("The line format is invalid. Line:\n%s", line));
        }

        // parse pack weight from the line
        String[] split = StringUtils.split(line, ':');
        BigDecimal packWeight = new BigDecimal(split[0].trim());
        if (packWeight.floatValue() > 100) {
            throw new APIException(String.format("Pack weight must not be over 100. Actual weight: %06f", packWeight.floatValue()));
        }

        // parse things from the line
        Matcher thingsMatcher = thingRegex.matcher(line);
        List<String> allMatches = new ArrayList<>();
        while (thingsMatcher.find()) {
            allMatches.add(thingsMatcher.group());
        }
        List<PackThing> things = allMatches.stream()
                .map(item -> {
                    String[] values = StringUtils.split(item, ',');
                    int index = Integer.parseInt(values[0].trim());
                    BigDecimal weight = new BigDecimal(values[1].trim());
                    if (weight.floatValue() > 100) {
                        throw new APIException(String.format("Thing weight must not be over 100. Actual weight: %06f", packWeight));
                    }
                    BigDecimal price = new BigDecimal(StringUtils.strip(values[2], "€"));
                    if (price.floatValue() > 100) {
                        throw new APIException(String.format("Thing price must not be over 100. Actual price: %06f", packWeight));
                    }


                    return new PackThing(index, weight, price);
                })
                .collect(Collectors.toList());

        things.sort(Comparator.comparing(PackThing::getWeight));
        if (things.size() > 15) {
            throw new APIException(String.format("Number of things must not be over 15. Actual number: %d", things.size()));
        }

        return new PackTask(packWeight, things.toArray(new PackThing[0]));
    }
}
