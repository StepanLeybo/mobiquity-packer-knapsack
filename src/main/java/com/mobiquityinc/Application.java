package com.mobiquityinc;

import com.google.common.base.Preconditions;
import com.mobiquityinc.packer.Packer;

/**
 * Main application class for packer problem
 */
public class Application {
    public static void main(String[] args) {
        Preconditions.checkArgument(args.length != 0, "First argument must be input file path.");

        System.out.println(Packer.pack(args[0]));
    }
}
